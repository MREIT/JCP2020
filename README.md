# Fast evaluation of the Biot-Savart integral using FFT for electrical conductivity imaging
In this project, we present codes for fast and efficient technique to evaluate 
the Biot-Savart integral based on the fast Fourier transform (FFT). The method 
can calculate magnetic fields in realistic human head models 
in one minute on a standard computer, while keeping error below 2%.

![alt text](https://gitlab.gbar.dtu.dk/MREIT/JCP2020/raw/master/Figures/head_model_B_calc.png "head_phantom_B_calc")